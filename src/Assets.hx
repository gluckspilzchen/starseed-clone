import dn.heaps.slib.*;

class Assets {
	public static var fontPixel : h2d.Font;
	public static var fontTiny : h2d.Font;
	public static var fontSmall : h2d.Font;
	public static var fontMedium : h2d.Font;
	public static var fontLarge : h2d.Font;
	public static var tiles : SpriteLib;

	static var music : dn.heaps.Sfx;

	static var initDone = false;
	public static function init() {
		if( initDone )
			return;
		initDone = true;

		music = new dn.heaps.Sfx(hxd.Res.music.soundtrack);
		music.groupId = 1;
		setVolume(0.05);

		fontPixel = hxd.Res.fonts.minecraftiaOutline.toFont();
		fontTiny = hxd.Res.fonts.barlow_condensed_medium_regular_9.toFont();
		fontSmall = hxd.Res.fonts.barlow_condensed_medium_regular_11.toFont();
		fontMedium = hxd.Res.fonts.barlow_condensed_medium_regular_17.toFont();
		fontLarge = hxd.Res.fonts.barlow_condensed_medium_regular_32.toFont();
		tiles = dn.heaps.assets.Atlas.load("atlas/tiles.atlas");
		tiles.defineAnim("heroRun", "4-5, 6, 8-9, 0, 1, 3");
		tiles.defineAnim("heroStand", "4, 0");
	}

	public static function getVolume() {
		return dn.heaps.Sfx.getGroupVolume(1);
	}
	public static function setVolume(v) {
		dn.heaps.Sfx.setGroupVolume(1, v);
	}
	public static function playMusic() {
		music.play(true);
	}

	public static function toggleMusicPause() {
		music.togglePlayPause();
	}
}
