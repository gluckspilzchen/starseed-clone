package ent;
import ent.Types.Seed;
import ent.Types.BlockType;
import haxe.ds.Option;

class BlockSpawner extends Entity {

	public static var ALL : Array<BlockSpawner> = [];

	var s : Seed;
	var b : ent.Block;
	var isFirst : Bool;
	var steps: Int;

	public function getTime(t) {
		var bdata =
			switch (t) {
				case Pink:
					Data.blocks.get(pink);
				case Orange:
					Data.blocks.get(orange);
				case Purple:
					Data.blocks.get(purple);
			}

		return bdata.spawnTime;
	}

	public function new (x, y, t:Seed, b:ent.Block, isFirst : Bool, steps) {
		super(x, y);

		s = t;

		isFixed = true;
		var g = new h2d.Graphics(spr);
		g.lineStyle(1, ent.Types.Util.getSeedColor(t));
		g.drawRect(-Const.GRID/2,-Const.GRID,Const.GRID,Const.GRID);

		this.b = b;
		this.isFirst = isFirst;
		this.steps = steps;

		level.addOccupied(cx, cy);

		b.addBlockSpawner(this);

		cd.setS("growing", getTime(t));
		ALL.push(this);
	}


	override function dispose() {
		super.dispose();

		level.removeOccupied(cx, cy);
		ALL.remove(this);
	}

	override function update() {
		super.update();

		if (!cd.has("growing")) {
			b.grown(isFirst);
			var nb = new Block(cx, cy, Seedable(s));
			game.hero.repel(nb);
			growMore(b, nb);
		}


	}

	function growMore(ob: ent.Block, nb:ent.Block) {
		var ncx, ncy;
		if (steps != 0)
			switch (s) {
				case Pink:
					ncx = cx;
					ncy = cy - 1;
					if (!level.isOccupied(ncx, ncy))
							new BlockSpawner(ncx, ncy, s, nb, false, steps);
				case Orange:
					var dx = cx - ob.cx;
					var dy = cy - ob.cy;
					ncx = cx + dx;
					ncy = cy + dy;
					if (!level.isOccupied(ncx, ncy))
							new BlockSpawner(ncx, ncy, s, nb, false, steps - 1);
				case Purple:
					var dx = cx - ob.cx;
					var dy = cy - ob.cy;
					if (dx != 0) {
						ncx = cx;
						ncy = cy + 1;
						if (!level.isOccupied(ncx, ncy))
							new BlockSpawner(ncx, ncy, s, nb, false, steps - 1);
						ncy = cy - 1;
						if (!level.isOccupied(ncx, ncy))
							new BlockSpawner(ncx, ncy, s, nb, false, steps - 1);
					}
					else {
						ncx = cx + 1;
						ncy = cy;
						if (!level.isOccupied(ncx, ncy))
							new BlockSpawner(ncx, ncy, s, nb, false, steps - 1);
						ncx = cx - 1;
						if (!level.isOccupied(ncx, ncy))
							new BlockSpawner(ncx, ncy, s, nb, false, steps - 1);
					}
		}
		destroy();
	}
}
