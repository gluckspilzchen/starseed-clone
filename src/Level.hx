import ent.Types.BlackBlockType;

enum Coord { Point (x : Int, y : Int); }

class Level extends dn.Process {
	public var game(get,never) : Game; inline function get_game() return Game.ME;
	public var fx(get,never) : Fx; inline function get_fx() return Game.ME.fx;

	public var wid(get,never) : Int; inline function get_wid() return 64;
	public var hei(get,never) : Int; inline function get_hei() return 64;

	public var spawnX : Int;
	public var spawnY : Int;

	public var level : World_Level;

	var invalidated = true;

	var collisions : Map<Coord,ent.Block> = new Map();
	var occupied : Map<Coord,Bool> = new Map();
	var blackBlocks : Map<Coord,Bool> = new Map();
	var locks : Map<Coord,ent.Lock> = new Map();
	var emptyBlocks : Map<Coord,ent.EmptyBlock> = new Map();

	var initGrid : Map<Coord, Bool> = new Map();

	public var isFlipped : Bool;

	public function new(l : World_Level) {
		super(Game.ME);
		createRootInLayers(Game.ME.scroller, Const.DP_BG);

		level = l;

		var spawn = l.l_Blocks.all_Spawn[0];

		isFlipped = false;

		spawnX = spawn.cx;
		spawnY = spawn.cy;
	}

	public function initLevel() {
		for (b in level.l_Blocks.all_Block) {
			new ent.Block(b.cx, b.cy, ent.Types.BlockType.Brown);
		}

		for (b in level.l_Blocks.all_BlackBlock) {
			new ent.BlackBlock(b.cx, b.cy, b.f_type);
		}
	}

	public inline function isValid(cx,cy) return cx>=0 && cx<wid && cy>=0 && cy<hei;
	public inline function coordId(cx,cy) return cx + cy*wid;


	public function render() {
	}

	public function hasCollision(cx, cy) : Bool {
		if (!isFlipped)
			return collisions.exists(Point(cx, cy));
		else
			return !hasBlackBlock(cx, cy);
	}

	public function hasBlackBlock(cx, cy) : Bool {
		return blackBlocks.exists(Point(cx, cy));
	}

	public function isOccupied(cx, cy) : Bool {
		return occupied.exists(Point(cx, cy)) || hasCollision(cx, cy) || hasBlackBlock(cx, cy);
	}

	public function getBlock(cx, cy) : ent.Block {
		return collisions.get(Point(cx, cy));
	}

	public function getEmptyBlock(cx, cy) : ent.EmptyBlock {
		return emptyBlocks.get(Point(cx, cy));
	}

	public function getLock(cx, cy) : ent.Lock {
		return locks.get(Point(cx, cy));
	}

	override function update() {
		super.update();
		var h = game.hero;
		var c = game.camera;

		var wid = M.ceil(c.wid / Const.GRID);
		var hei = M.ceil(c.hei / Const.GRID);

		var startX = h.cx - M.ceil(wid/2);
		var startY = h.cy - M.ceil(hei/2);
		var endX = startX + wid;
		var endY = startY + hei;

		if (!initGrid.exists(Point(startX, startY)) || !initGrid.exists(Point(startX, endY)) || !initGrid.exists(Point(endX, startY)) || !initGrid.exists(Point(endX, endY)))  {
			for (i in startX...(endX + 1)) {
				for (j in startY...(endY + 1)) {
					var p = initGrid.get(Point(i, j));
					if (p == null || !p) {
						generateBlackBlock(i, j);
						initGrid.set(Point(i, j), true);
					}
				}
			}
		}
	}

	function generateBlackBlock(i : Int, j: Int) {
		var d = M.dist(i, j, spawnX, spawnY);

		var r = M.frand();
		var bbt = if (r > 0.10) { Key; } else { StrongLock; }

		if (d > 128) {
			var r = M.frand();

			bbt = if (r > 0.95) { bbt; } else { Void; }

			r = M.frand();
			if (r > 0.25) {
				var b = new ent.BlackBlock(i, j, bbt);
				if (isFlipped)
					b.flip();
			}
		}
		else if (d > 24) {
			var r = M.frand();

			if (r > 0.995) {
				var b = new ent.BlackBlock(i, j, bbt);
				if (isFlipped)
					b.flip();
			}
		}
	}

	override function postUpdate() {
		super.postUpdate();

		if( invalidated ) {
			invalidated = false;
			render();
		}
	}

	public function addCollision(b:ent.Block) {
		collisions.set(Point(b.cx, b.cy), b);
	}

	public function addOccupied(cx, cy) {
		occupied.set(Point(cx, cy), true);
	}

	public function addLock(l:ent.Lock) {
		locks.set(Point(l.cx, l.cy), l);
	}

	public function addBlackBlock(cx, cy) {
		blackBlocks.set(Point(cx, cy), true);
	}

	public function addEmptyBlock(eb:ent.EmptyBlock) {
		emptyBlocks.set(Point(eb.cx, eb.cy), eb);
	}

	public function removeCollision(cx, cy) {
		collisions.remove(Point(cx, cy));
	}

	public function removeOccupied(cx, cy) {
		occupied.remove(Point(cx, cy));
	}

	public function removeBlockBlock(cx, cy) {
		blackBlocks.remove(Point(cx, cy));
	}

	public function removeEmptyBlock(cx, cy) {
		emptyBlocks.remove(Point(cx, cy));
	}

	public function flip() {
		if (isFlipped)
			return;

		engine.backgroundColor = 0x090909;

		isFlipped = true;
		for (bs in ent.BlockSpawner.ALL) bs.destroy();
		for (b in ent.Block.ALL) {
			new ent.BlackBlock(b.cx, b.cy);
			b.destroy();
		}

		for (eb in ent.EmptyBlock.ALL) {
			new ent.BlackBlock(eb.cx, eb.cy);
			eb.destroy();
		}

		for (bb in ent.BlackBlock.ALL) bb.flip();

		game.hero.flip();
	}
}
