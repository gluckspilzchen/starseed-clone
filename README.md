# About

This is a demo clone of starseed pilgrim:
http://www.starseedpilgrim.com/

This project is way for me to focus on learning how to use haxe/heaps, without
having to deal with the game-design part of video game programming.

If you enjoy playing this demo, do consider buying the original game, it is
both challenging and delightful.
